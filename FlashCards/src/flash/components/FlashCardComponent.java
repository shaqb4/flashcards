/**
 * 
 */
package flash.components;

import javafx.scene.layout.StackPane;

import java.io.IOException;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.text.Text;

/**
 * @author Shahked Bleicher
 *
 */
public class FlashCardComponent extends StackPane {

	@FXML 
	private Text questionText;
	@FXML 
	private Text answerText;
	@FXML
	private BooleanProperty isShowingQuestion;
	
	
	public FlashCardComponent() {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("FlashCardComponent.fxml"));
		loader.setRoot(this);
		loader.setController(this);
		this.isShowingQuestion = new SimpleBooleanProperty(true);
		
		try {
			loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void initialize() {
		this.setPrefWidth(250);
		this.setPrefHeight(150);
	}

	@FXML
	public void switchDisplay() {
		if (this.isShowingQuestion.get()) {
			this.answerText.setVisible(true);
			this.questionText.setVisible(false);
			this.isShowingQuestion.set(false);
		} else {
			this.answerText.setVisible(false);
			this.questionText.setVisible(true);
			this.isShowingQuestion.set(true);
		}
	}

	/**
	 * @return the questionText
	 */
	public String getQuestion() {
		return questionText.getText();
	}


	/**
	 * @param questionText the questionText to set
	 */
	public void setQuestion(String questionText) {
		this.questionText.setText(questionText);
	}


	/**
	 * @return the answerText
	 */
	public String getAnswer() {
		return answerText.getText();
	}


	/**
	 * @param answerText the answerText to set
	 */
	public void setAnswer(String answerText) {
		this.answerText.setText(answerText);
	}


	/**
	 * @return the isShowingQuestion
	 */
	public boolean getIsShowingQuestion() {
		return isShowingQuestion.get();
	}
	
	/**
	 * @return the isShowingQuestion property
	 */
	public BooleanProperty isShowingQuestionProperty() {
		return this.isShowingQuestion;
	}


	/**
	 * @param isShowingQuestion the isShowingQuestion to set
	 */
	public void setIsShowingQuestion(boolean isShowingQuestion) {
		this.isShowingQuestion.set(isShowingQuestion);
	}

}
