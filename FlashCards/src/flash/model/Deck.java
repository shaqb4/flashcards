/**
 * 
 */
package flash.model;

import java.util.Optional;
import java.util.Random;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * @author Shahked Bleicher
 *
 */
public class Deck {

	private ObservableList<FlashCard> cards;
	
	private int currentIndex;
	
	private String title;
	
	public Deck() {
		this.cards = FXCollections.observableArrayList();
		this.currentIndex = 0;
		this.title = "A Deck";
		//this.cards.add(new FlashCard("Hola", "Hello", true));
	}
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	public Optional<FlashCard> getCurrentCard() {
		if (this.currentIndex >= 0 && this.currentIndex < this.cards.size()) {
			return Optional.of(this.cards.get(this.currentIndex));
		} else {
			return Optional.empty();
		}
	}
	
	public Optional<FlashCard> getNextCard() {
		if (this.currentIndex+1 >= 0 && this.currentIndex+1 < this.cards.size()) {
			this.currentIndex++;
			return Optional.of(this.cards.get(this.currentIndex));
		} else {
			return Optional.empty();
		}
	}
	
	public Optional<FlashCard> getPrevCard() {
		if (this.currentIndex-1 >= 0 && this.currentIndex-1 < this.cards.size()) {
			this.currentIndex--;
			return Optional.of(this.cards.get(this.currentIndex));
		} else {
			return Optional.empty();
		}
	}
	
	public Optional<FlashCard> getRandomCard() {
		Random rand = new Random();
		int randNum = rand.nextInt(this.cards.size());
		if (randNum >= 0 && randNum < this.cards.size()) {
			this.currentIndex = randNum;
			return Optional.of(this.cards.get(this.currentIndex));
		} else {
			return Optional.empty();
		}
	}
	
	public void reset() {
		this.currentIndex = 0;
	}
	
	public void addCard(String q, String ans, boolean isAsking) {
		this.cards.add(new FlashCard(q, ans, isAsking));
	}
	
	public void addCard(FlashCard card) {
		this.cards.add(card);
	}
}
