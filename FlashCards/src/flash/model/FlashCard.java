/**
 * 
 */
package flash.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * @author Shahked Bleicher
 *
 */
public class FlashCard {

	private StringProperty question;
	private StringProperty answer;
	private BooleanProperty isAsking;
	
	public FlashCard() {
		this("", "", true);
	}
	
	public FlashCard(String question, String answer, boolean asking) {
		this.question = new SimpleStringProperty(question);
		this.answer = new SimpleStringProperty(answer);
		this.isAsking = new SimpleBooleanProperty(asking);
	}
	
	/**
	 * @return the question property
	 */
	public StringProperty questionProperty() {
		return question;
	}

	/**
	 * 
	 * @return The question
	 */
	public String getQuestion() {
		return this.question.get();
	}
	
	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question.set(question);
	}

	/**
	 * @return the answer property
	 */
	public StringProperty answerProperty() {
		return answer;
	}

	/**
	 * 
	 * @return The answer
	 */
	public String getAnswer() {
		return this.answer.get();
	}
	
	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(String answer) {
		this.answer.set(answer);
	}

	/**
	 * @return the isAsking property
	 */
	public BooleanProperty isAskingProperty() {
		return isAsking;
	}

	/**
	 * 
	 * @return Whether isAsking
	 */
	public boolean getIsAsking() {
		return this.isAsking.get();
	}
	
	/**
	 * @param isAsking the isAsking to set
	 */
	public void setIsAsking(boolean isAsking) {
		this.isAsking.set(isAsking);
	}

}
