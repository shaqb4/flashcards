package flash;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;


public class Main extends Application {
	
	private Stage primaryStage;
	private BorderPane rootLayout;
	
	public void initRootLayout() {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("view/FlashCardApp.fxml"));
			this.rootLayout = (BorderPane)loader.load();
			
			Scene scene = new Scene(this.rootLayout);
			this.primaryStage.setScene(scene);
			
			this.primaryStage.show();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void start(Stage primaryStage) {
		try {
			this.primaryStage = primaryStage;
			this.primaryStage.setTitle("Flash Cards");
			
			this.initRootLayout();
			
			//BorderPane root = (BorderPane)FXMLLoader.load(getClass().getResource("FlashCardApp.fxml"));
			//Scene scene = new Scene(root,400,400);
			//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			//primaryStage.setScene(scene);
			//primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
