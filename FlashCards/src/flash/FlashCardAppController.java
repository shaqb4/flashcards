package flash;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Collectors;

import flash.components.FlashCardComponent;
import flash.model.Deck;
import flash.model.FlashCard;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.util.StringConverter;

public class FlashCardAppController {

	@FXML 
	private Button randomButton;
	@FXML 
	private Button resetButton;
	/*@FXML 
	private RadioButton questionToggle;
	@FXML 
	private RadioButton answerToggle;
	@FXML 
	private ToggleGroup showFlashToggle;*/
	@FXML 
	private ChoiceBox<Deck> deckSelect;
	@FXML 
	private BorderPane rootPane;
	@FXML 
	private StackPane centerPane;
	@FXML
	private FlashCardComponent flashCard;
	@FXML 
	private Button prevButton;
	@FXML 
	private Button nextButton;
	@FXML 
	private StackPane leftPane;
	@FXML 
	private StackPane rightPane;
	
	private ObservableList<Deck> decks;
	
	private int currentDeck;
	
	
	
	
	public FlashCardAppController() {
		this.decks = FXCollections.observableArrayList();
		this.readData("data");
		this.currentDeck = 0;
	}
	
	private void readData(String dir) {
		try {
			Files.list(Paths.get(dir))
				.forEach(path -> {
					Deck deck = new Deck();
					deck.setTitle(path.getFileName().toString());
					try {
						Files.lines(path, StandardCharsets.UTF_8).map((String str) -> {
							String[] qa = str.split("\t");
							FlashCard card = new FlashCard(qa[0], qa[1], true);
							return card;
						}).forEach(card -> deck.addCard(card));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					this.decks.add(deck);
				});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@FXML
	private void initialize() {
		this.deckSelect.setItems(this.decks);
		this.deckSelect.getSelectionModel().selectedIndexProperty().addListener((observable, oldVal, newVal) -> {
			this.currentDeck = newVal.intValue();
			this.resetDeck();
		});
		this.deckSelect.setConverter(new StringConverter<Deck>() {

			@Override
			public String toString(Deck object) {
				return object.getTitle();
			}

			@Override
			public Deck fromString(String string) {
				return decks.filtered(d -> d.getTitle().equals(string)).get(0);
			}
			
		});
		
		
		if (this.decks.size() > 0) {
			Optional<FlashCard> curr = this.decks.get(this.currentDeck).getCurrentCard();
			
			curr.ifPresent(card -> {
				this.flashCard.setQuestion(card.getQuestion());
				this.flashCard.setAnswer(card.getAnswer());
				this.flashCard.setIsShowingQuestion(card.getIsAsking());
			});
		}
	}
	
	@FXML
	public void prevCard() {
		Optional<FlashCard> prev = this.decks.get(this.currentDeck).getPrevCard();
		
		prev.ifPresent(card -> {
			this.flashCard.setQuestion(card.getQuestion());
			this.flashCard.setAnswer(card.getAnswer());
			this.flashCard.setIsShowingQuestion(card.getIsAsking());
		});
	}
	
	@FXML
	public void nextCard() {
		Optional<FlashCard> next = this.decks.get(this.currentDeck).getNextCard();
		
		next.ifPresent(card -> {
			this.flashCard.setQuestion(card.getQuestion());
			this.flashCard.setAnswer(card.getAnswer());
			this.flashCard.setIsShowingQuestion(card.getIsAsking());
		});
	}
	
	@FXML
	public void randomCard() {
		Optional<FlashCard> random = this.decks.get(this.currentDeck).getRandomCard();
		
		random.ifPresent(card -> {
			this.flashCard.setQuestion(card.getQuestion());
			this.flashCard.setAnswer(card.getAnswer());
			this.flashCard.setIsShowingQuestion(card.getIsAsking());
		});
	}
	
	@FXML
	public void resetDeck() {
		this.decks.get(this.currentDeck).reset();
		Optional<FlashCard> curr = this.decks.get(this.currentDeck).getCurrentCard();
		
		curr.ifPresent(card -> {
			this.flashCard.setQuestion(card.getQuestion());
			this.flashCard.setAnswer(card.getAnswer());
			this.flashCard.setIsShowingQuestion(card.getIsAsking());
		});
	}
	
}
